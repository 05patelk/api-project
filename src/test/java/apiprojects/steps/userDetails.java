package apiprojects.steps;

import http.Request;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.ValidatableResponse;
import org.junit.Assert;

public class userDetails {

    private static ValidatableResponse apiResponse;


    @Given("^I have requested details of user with id of \"([^\"]*)\"$")
    public void iHaveRequestedDetailsOfUserWithIdOf(String id)  {
        apiResponse = Request.get(String.format("/user/%s", id));
    }

    @Then("^I get a OK response$")
    public void iGetAOKResponse()  {
        Assert.assertEquals(200, apiResponse.extract().statusCode());
    }



    @And("^I should get back correct user details for \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\";$")
    public void iShouldGetBackCorrectUserDetailsFor(String userFirstName, String userLastName, String emailAddress) throws Throwable {
        Assert.assertEquals(userFirstName, apiResponse.extract().body().jsonPath().get("first_name"));
        Assert.assertEquals(userLastName, apiResponse.extract().body().jsonPath().get("last_name") );
        Assert.assertEquals(emailAddress, apiResponse.extract().body().jsonPath().get("email") );
    }

    @Then("^I get a Not Found Response$")
    public void iGetANotFoundResponse()  {
            Assert.assertEquals(404, apiResponse.extract().statusCode());
        }

}
