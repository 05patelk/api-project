package apiprojects.steps;

import http.Request;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.ValidatableResponse;
import org.junit.Assert;

public class city {


   private static ValidatableResponse apiResponse;

    @Given("^I have requested for a city$")
    public void iHaveRequestedForACity() {
        apiResponse = Request.get("/city/Leeds/users");
    }

    @Then("^I get a OK responses$")
    public void iGetAOKResponse()  {
        Assert.assertEquals(200, apiResponse.extract().statusCode());
    }
}

