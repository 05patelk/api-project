Feature: Get User Details

  Scenario Outline: I get a back user details of valid user
    Given I have requested details of user with id of "<number>"
    Then I get a OK response
    And I should get back correct user details for "<FirstName>", "<LastName>", "<EmailAddress>";
    Examples:
    |number| FirstName | LastName | EmailAddress                |
    |1     | Maurise   | Shieldon | mshieldon0@squidoo.com      |
    |2     | Bendix    | Halgarth | bhalgarth1@timesonline.co.uk|


    Scenario: Error appears when I request an invalid user
      Given I have requested details of user with id of "1237129837"
      Then I get a Not Found Response