package http;



import io.restassured.response.ValidatableResponse;


import static io.restassured.RestAssured.given;

public class Request {

    private static ValidatableResponse response;

    public static ValidatableResponse get( String serviceEndPoint) {
        response = given()
                .get(String.format("http://bpdts-test-app-v2.herokuapp.com%s", serviceEndPoint))
                .then();
        return response;
    }
}
